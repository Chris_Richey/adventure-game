﻿using UnityEngine;
using System.Collections;

public class moveLight : MonoBehaviour {

	public float thrust;
	public Rigidbody rb;

	void Start(){
		rb = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		rb.AddForce (transform.forward * thrust);
	}
}
