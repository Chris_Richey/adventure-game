﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pickUp : MonoBehaviour {
	public AudioSource m_pickup;
	public Image redkey;
	public Image bluekey;
	public Image greenkey;
	public Image yellowkey;
	public Image endimage;
	//public Camera player;
	public Camera end;
	public GameObject fps;
	public static bool redkeybool = false;
	public static bool bluekeybool = false;
	public static bool greenkeybool = false;
	public static bool yellowkeybool = false;

	public AudioSource coinSound;

	// Use this for initialization
	void Start () 
	{
		redkey.gameObject.SetActive (false);
		bluekey.gameObject.SetActive (false);
		greenkey.gameObject.SetActive(false);
		yellowkey.gameObject.SetActive (false);
		endimage.gameObject.SetActive (false);
		end.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("coin")) {
			coinSound.Play ();

		}
		if (other.gameObject.CompareTag ("redkeyobject")) {
			redkeybool = true;
			//	AudioClip.(pickUp);
			GameObject reddoor = GameObject.FindGameObjectWithTag("reddoor");

			other.gameObject.SetActive (false);
			redkey.gameObject.SetActive (true);

			reddoor.gameObject.transform.eulerAngles = new Vector3(
				gameObject.transform.eulerAngles.x,
				90f,
				gameObject.transform.eulerAngles.z
			);

			m_pickup.Play (); 
		}
		if (other.gameObject.CompareTag ("greenkeyobject")) {
			greenkeybool = true;
			//	AudioClip.(pickUp);
			GameObject greendoor = GameObject.FindGameObjectWithTag("greendoor");

			other.gameObject.SetActive (false);
			greenkey.gameObject.SetActive (true);
			//greendoor.gameObject.transform.eulerAngles.y = 90;
			//greendoor.gameObject.transform.x.position = 133.1;
			//greendoor.gameObject.transform.y.position = -371.76;
			//greendoor.gameObject.transform.z.position = 422.6;
			greendoor.gameObject.transform.eulerAngles = new Vector3(
				gameObject.transform.eulerAngles.x,
				90f,
				gameObject.transform.eulerAngles.z
			);

			/*greendoor.gameObject.transform.position = new Vector3(
				133.1f,
				-371.76f,
				422.6f
			);*/

			m_pickup.Play (); 
		}
		if (other.gameObject.CompareTag ("bluekeyobject")) {
			bluekeybool = true;
			//	AudioClip.(pickUp);
			GameObject bluedoor = GameObject.FindGameObjectWithTag("bluedoor");

			other.gameObject.SetActive (false);
			bluekey.gameObject.SetActive (true);
			bluedoor.gameObject.transform.eulerAngles = new Vector3(
				gameObject.transform.eulerAngles.x,
				-90f,
				gameObject.transform.eulerAngles.z
			);
			m_pickup.Play (); 
		}
		if (other.gameObject.CompareTag ("yellowkeyobject")) {
			yellowkeybool = true;
			//	AudioClip.(pickUp);
			//GameObject yellowdoor = GameObject.FindGameObjectWithTag("yellowdoor");

			other.gameObject.SetActive (false);
			//yellowkey.gameObject.SetActive (true);

			yellowkey.gameObject.SetActive (true);

			m_pickup.Play (); 
		}

		if (other.gameObject.CompareTag ("energy1")){
			other.gameObject.SetActive (false);
			m_pickup.Play (); 
			global.HaveWeaponTwo = true;
			global.Light_Amo1 += 10;
		}
		if (other.gameObject.CompareTag ("energy2")){
			other.gameObject.SetActive (false);
			m_pickup.Play (); 
			global.HaveWeaponThree = true;
			global.Light_Amo2 += 10;
		}
		if (other.gameObject.CompareTag ("energy3")){
			other.gameObject.SetActive (false);
			m_pickup.Play (); 
			global.HaveWeaponFour = true;
			global.Light_Amo3 += 10;
		}
	}

}