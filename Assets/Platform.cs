﻿using UnityEngine;
using System.Collections;



public class Platform : MonoBehaviour {

	public GameObject cplatform;
	public GameObject dplatform;

	// Use this for initialization
	void Start () {
		cplatform.gameObject.SetActive (false);
		dplatform.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if (global.adoor == true && global.bdoor == true && global.cdoor == true && global.ddoor == true) {
			cplatform.gameObject.SetActive (true);
			dplatform.gameObject.SetActive (true);
		}
	}
}
