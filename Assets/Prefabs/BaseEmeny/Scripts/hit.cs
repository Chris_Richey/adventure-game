﻿using UnityEngine;
using System.Collections;

public class hit : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Quaternion angle = Quaternion.Euler (0, other.transform.rotation.y, 0);
			this.transform.rotation = angle;
			Destroy (other.gameObject);
		}
	}
}
