﻿using UnityEngine;
using System.Collections;

public class hit3 : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Quaternion angle3 = Quaternion.Euler (0, other.transform.rotation.y, 0);
			this.transform.rotation = angle3;
			Destroy (other.gameObject);
		}
	}
}
