﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoinScript : MonoBehaviour {

	public AudioSource sound;

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Player")){
			sound.Play ();
			global.Coins++;
			this.gameObject.SetActive (false);
		}
	}
}
