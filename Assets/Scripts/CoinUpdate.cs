﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoinUpdate : MonoBehaviour {

	public Text txt;

	void Start () {
		txt = gameObject.GetComponent<Text>(); 
		txt.text = "Coins: " + global.Coins + "/40";
	}
	
	// Update is called once per frame
	void Update () {
		txt.text="Coins: " + global.Coins + "/40";
	}
}