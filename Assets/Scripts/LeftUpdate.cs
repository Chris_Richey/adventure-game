﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeftUpdate : MonoBehaviour {

	public Text txt;

	void Start () {
		txt = gameObject.GetComponent<Text>(); 
		txt.text="Light " + UnityStandardAssets.Characters.FirstPerson.FirstPersonController.shotSelect;
	}

	// Update is called once per frame
	void Update () {
		txt.text="Light " + UnityStandardAssets.Characters.FirstPerson.FirstPersonController.shotSelect;
	}
}