﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	public static bool pauseGame = false;

	void Start(){
		pauseGame = false;
	}

	void Update () {
		if (Input.GetKeyDown ("t")) {
			global.Light_Amo += 1000;
		}
		if (Input.GetKeyDown ("p")) {
			pauseGame = true;
		}
		if (Input.GetKeyDown ("m")) {
			Application.LoadLevel (0);
		}

		if (pauseGame) {
			Time.timeScale = 0;

			PauseMenuScript.MenuTextVisable = true;
			PauseMenuScript.PlayTextVisable = true;
			PauseMenuScript.PauseMenuVisable = true;
			Cursor.visible = true;
			Screen.lockCursor = false;
		}

		if (!pauseGame) {
			Time.timeScale = 1;

			PauseMenuScript.MenuTextVisable = false;
			PauseMenuScript.PlayTextVisable = false;
			PauseMenuScript.PauseMenuVisable = false;
			PauseMenuScript.YesNoVisable = false;
			Cursor.visible = false;
			Screen.lockCursor = true;
		}
	}
}
