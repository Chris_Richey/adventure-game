﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;

public class PauseMenuScript : MonoBehaviour {
	public Canvas YesNo;
	public Canvas PauseMenu;
	public Button PlayText;
	public Button MenuText;
	public static bool YesNoVisable;
	public static bool PauseMenuVisable;
	public static bool PlayTextVisable;
	public static bool MenuTextVisable;
	public GameObject enemies;
	public GameObject Player;

	public static bool LoadSave = false;

	void Start () {
		YesNo = YesNo.GetComponent<Canvas> ();
		PauseMenu = PauseMenu.GetComponent<Canvas> ();
		MenuText = MenuText.GetComponent<Button> ();
		PlayText = PlayText.GetComponent<Button> ();
		YesNoVisable = false;
		MenuTextVisable = false;
		PlayTextVisable = false;
		PauseMenuVisable = false;
	}

	void Update () {
		YesNo.enabled = YesNoVisable;
		MenuText.enabled = MenuTextVisable;
		PlayText.enabled = PlayTextVisable;
		PauseMenu.enabled = PauseMenuVisable;
	}

	public void ExitPress(){
		YesNoVisable = true;
		MenuTextVisable = false;
		PlayTextVisable = false;
	}

	public void ExitGame (){
		Application.LoadLevel (0);
	}

	public void ExitPause (){
		Pause.pauseGame = false;
	}

	public void CancelPress (){
		YesNoVisable = false;
		MenuTextVisable = true;
		PlayTextVisable = true;
		PauseMenuVisable = true;
	}
}
