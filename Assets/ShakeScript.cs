﻿using UnityEngine;
using System.Collections;

public class ShakeScript : MonoBehaviour {

	public Camera camera; // set this via inspector
	public float shake = 0f;
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;

	public float shakeAngle;
	public float shakeLength;

	// Use this for initialization
	void Start () {
	
	}

	private float ShakeY = 0.8f;
	private float ShakeYSpeed = 1.0f;

	public void setShake(float someY)
	{
		ShakeY = someY;
	}

	void Update()
	{
		if (global.endg == true) {
			Vector2 _newPosition = new Vector2 (0, ShakeY);
			if (ShakeY < 0) {
				ShakeY *= ShakeYSpeed;
			}
			ShakeY = -ShakeY;
			camera.transform.Translate (_newPosition, Space.Self);
		}
	}

}
