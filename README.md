# README #

### What is this repository for? ###

- This was a project for my Software Engineering class at PLNU.
- It is a game to played on your computer which is made in Unity with coding in C#.
- The other people who worked on it are Erik Gaustad, Jack Higgins, Andrew Taylor, and Benjamin Khoshaba.


### How do I get set up? ###

- Download the repository.
- Open the files in Unity.
- Run the program on the Main Menu scene.


### Who do I talk to? ###

- Chris Richey is the owner and admin of this repo and game. Please email me at chrisrichey1995@pointloma.edu with any questions.